# qemu
[原理](https://www.imooc.com/article/37495)


#### MCU
[QEMU 仿真 STM32 CubeMX 生成的项目](https://www.cnblogs.com/schips/p/12400576.html)

[The xPack QEMU Arm](https://xpack.github.io/qemu-arm/)
The following Cortex-M3 and Cortex-M4 microcontrollers are supported:

STM32F103RB
STM32F107VC
STM32F405RG
STM32F407VG
STM32F407ZG
STM32F429ZI
STM32L152RE

* x86_64 上模拟Arch64 
#### [安装qemu-aarch64](https://blog.csdn.net/chenxiangneu/article/details/78955462)
```
apt-get install qemu-system-aarch64
qemu-img create centos7-arm64.img 16G
wget https://mirrors.tuna.tsinghua.edu.cn/centos-altarch/7.9.2009/isos/aarch64/CentOS-7-aarch64-Minimal-2009.iso
wget http://releases.linaro.org/components/kernel/uefi-linaro/16.02/release/qemu64/QEMU_EFI.fd

qemu-system-aarch64 -m 2048 -cpu cortex-a57 -smp 2 -M virt -bios QEMU_EFI.fd -display vnc=0.0.0.0:80 \
-drive if=none,file=CentOS-7-aarch64-Minimal-2009.iso,id=cdrom,media=cdrom \
-device virtio-scsi-device -device scsi-cd,drive=cdrom \
-drive if=none,file=centos7-arm64.img,id=hd0 -device virtio-blk-device,drive=hd0

```
[Cenos7.9 aarch64 ISO](https://mirrors.tuna.tsinghua.edu.cn/centos-altarch/7.9.2009/isos/aarch64/ )

[rootfs制作](https://blog.csdn.net/liuhangtiant/article/details/80304267)

[zos](https://github.com/robinzh/zos)

[qemu网络配置](https://blog.csdn.net/rikeyone/article/details/106767540)

* Arch64 上模拟x86_64 [QEMU官网](https://www.qemu.org/download/)


```
wget https://download.qemu.org/qemu-5.0.0.tar.xz
./configure --enable-kvm --target-list=x86_64-softmmu,x86_64-linux-user
qemu-system-x86_64 -m 3500 -smp 4 -boot d -hda deepin.img -cdrom deepin-20Beta-desktop-amd64.iso -display vnc=0.0.0.0:80 
--enable-kvm 可以使用kvm加速， 暂时可能没法使用
```
