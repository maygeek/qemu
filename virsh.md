* virsh
```
apt-get install qemu-system-x86-64 libvirt-daemon qemu qemu-kvm libvirt-clients libvirt-daemon-system libvirt-daemon-system-systemd
qemu-img create -f qcow2 centos7.img 30G 
qemu-img amend -f qcow2 -o compat=0.10 centos7.img 
virsh define centos7.xml
virsh destroy
virsh undefine
virsh start centos7
virsh edit centos7
cat centos7.xml

virsh vncdisplay centos
```

* xml
```
<domain type='kvm'>  
  <name>centos7-2</name>  
  <uuid>d06d7213-9b3d-f32e-f54a-70a82115242e</uuid>  
  <memory unit='KiB'>8388608</memory>  
  <currentMemory unit='KiB'>8388608</currentMemory>  
  <vcpu placement='static'>4</vcpu>  
  <os>  
    <type arch='x86_64' machine='pc'>hvm</type>  
    <boot dev='hd'/>  
     <bootmenu enable='yes'/>  
  </os>  
  <features>  
    <acpi/>  
    <apic/>  
    <pae/>  
  </features>  
  <clock offset='utc'/>  
  <on_poweroff>destroy</on_poweroff>  
  <on_reboot>restart</on_reboot>  
  <on_crash>restart</on_crash>  
  <devices>  
    <emulator>/usr/libexec/qemu-kvm</emulator>  
    <disk type='file' device='disk'>  
      <driver name='qemu' type='qcow2' cache='none'/>  
      <source[color=darkred] file='/home/hao/centos7-2.img'/[/color]>  
      <target dev='vda' bus='virtio'/>  
    </disk>  
    <disk type='file' device='cdrom'>  
      <driver name='qemu' type='raw'/>  
      <source [color=darkred]file='/home/CentOS-7-x86_64-DVD-1503-01.iso'/>[/color]  
      <target dev='hdc' bus='ide'/>  
      <readonly/>  
      <address type='drive' controller='0' bus='1' target='0' unit='0'/>  
    </disk>  
    <controller type='usb' index='0'>  
      <address type='pci' domain='0x0000' bus='0x00' slot='0x01' function='0x2'/>  
    </controller>  
    <controller type='ide' index='0'>  
      <address type='pci' domain='0x0000' bus='0x00' slot='0x01' function='0x1'/>  
    </controller>  
    <interface type='bridge'>  
      <mac address='50:50:18:aa:83:81'/>  
      <source bridge='br0'/>  
      <model type='virtio'/>  
    </interface>  
    <serial type='pty'>  
      <target port='0'/>  
    </serial>  
    <console type='pty'>  
      <target type='serial' port='0'/>  
    </console>  
    <input type='mouse' bus='ps2'/>  
    <graphics type='vnc' port='5909' autoport='no' listen='0.0.0.0'>  
      <listen type='address' address='0.0.0.0'/>  
    </graphics>  
    <sound model='ich6'>  
      <address type='pci' domain='0x0000' bus='0x00' slot='0x04' function='0x0'/>  
    </sound>  
    <video>  
      <model type='cirrus' vram='9216' heads='1'/>  
      <address type='pci' domain='0x0000' bus='0x00' slot='0x02' function='0x0'/>  
    </video>  
    <memballoon model='virtio'>  
      <address type='pci' domain='0x0000' bus='0x00' slot='0x06' function='0x0'/>  
    </memballoon>  
  </devices>  
</domain>  
```

```
<domain type='kvm'>    
	<name>test</name>    
	<memory>1048576</memory>    
	<currentMemory>1048576</currentMemory>    
	<vcpu>2</vcpu>    
<os>      
	<type arch='x86_64' machine='pc'>hvm</type>      
	<boot dev='hd'/>   
</os>   
<features>   <acpi/>     <apic/>     <pae/>   </features>   
<clock offset='localtime'/>   
<on_poweroff>destroy</on_poweroff>   
<on_reboot>restart</on_reboot>   
<on_crash>destroy</on_crash>  
 
<devices>         
	<emulator>/usr/bin/qemu-system-x86_64</emulator>     

	<video>       
	<model type='vga' vram='16384' heads='1'>       </model>       
	<driver name='qemu'/>     
	</video>     

	<disk type='file' device='disk'>      
	<driver name='qemu' type='qcow2'/>       
	<source file='/home/xx/VM/xx.qcow2'/>       
	<target dev='hda' bus='ide'/>     
	</disk>    

	<interface type='bridge'>      
	<source bridge='virbr0'/>      
	<mac address="00:16:3e:5d:aa:a6"/>          
	<model type="e1000" />    
	</interface>    

	<input type='mouse' bus='ps2'/>     
	<graphics type='vnc' port='-1' autoport='yes' listen = '0.0.0.0' keymap='en-us'/>   
</devices>
</domain>

```
* img
```
qemu-img convert -f raw -O qcow2 xx.img xx.qcow2
```

* net