#!/bin/bash
URL="http://121.36.84.172/dailybuild/EBS-openEuler-Mainline/embedded_img/aarch64/qemu-aarch64-kernel6/20250310113015"

prepare()
{
   curl -LO "$URL/openeuler-image-qemu-aarch64-20250310113015.rootfs.cpio.gz"
   curl -LO "$URL/zImage"

}


setup_tap()
{
    ifname=$1
    #sudo ip tuntap del ${ifname} mode tap
    #sudo ip tuntap add ${ifname}  mode tap
    sudo ip link set ${ifname} up
    sudo ip link set ${ifname} master $2
}


bridge()
{
   echo "prepare"
   #brctl addbr br_ctrl
   #ifconfig br_ctrl 192.168.11.1/24
   # fconfig br_ctrl up

   #iptables -A FORWARD -i br_qemu -o enp1s0 -j ACCEPT
   #iptables -A FORWARD -o br_qemu -i enp1s0 -j ACCEPT
   #iptables -t nat -A POSTROUTING -s 192.168.122.0/24 -j MASQUERADE
}

start_br()
{
qemu-system-aarch64 -M virt-4.0 -m 1G -cpu cortex-a57 -nographic \
    -netdev bridge,br=br_ctrl,id=net0 -device virtio-net-pci,netdev=net0 \
    -kernel zImage \
    -initrd openeuler-image-qemu-aarch64-*.rootfs.cpio.gz
}

start_vm()
{
qemu-system-aarch64 -M virt-4.0 -m 1G -cpu cortex-a57 -nographic \
    -kernel zImage \
    -initrd openeuler-image-qemu-aarch64-*.rootfs.cpio.gz \
    -device virtio-net-device,netdev=tap0 \
    -netdev tap,id=tap0,script=./qemu-ifup
}

start_vm &

#setup_tap tap0 br-ctrl
