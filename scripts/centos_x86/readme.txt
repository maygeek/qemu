#wget -c https://mirrors.tuna.tsinghua.edu.cn/centos-altarch/7.9.2009/isos/aarch64/CentOS-7-aarch64-Minimal-2009.iso

apt-get install qemu-system-x86-64 libvirt-daemon qemu qemu-kvm libvirt-clients libvirt-daemon-system libvirt-daemon-system-systemd
qemu-img create -f qcow2 centos7.img 30G 
qemu-img amend -f qcow2 -o compat=0.10 centos7.img 
virsh define centos7.xml
virsh destroy
virsh undefine
virsh start centos7
virsh edit centos7
cat centos7.xml

virsh vncdisplay centos

virsh define  centos.xml