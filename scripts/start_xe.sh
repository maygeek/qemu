#!/bin/sh

if [ $# -ne 1 ];then
  echo "Usage: $0 network-mode[user|tap]"
  echo "e.g.: $0 user"
  echo "      $0 tap"
  exit
fi


if [ $1 == "user" ];then
  # User Networking Mode
  qemu-system-aarch64 -smp 4 -m 4096 -M virt -bios QEMU_EFI.fd -nographic 
       -device virtio-blk-device,drive=image 
       -drive if=none,id=image,file=xenial-server-cloudimg-arm64-uefi1.qcow2,format=qcow2 
       -device virtio-blk-device,drive=cloud 
       -drive if=none,id=cloud,file=cloud.img,format=raw 
       -netdev user,id=user0,hostfwd=tcp::2222-:22 -device virtio-net-device,netdev=user0 
       -enable-kvm -cpu host
elif [ $1 == "tap" ];then
  # Tap Networking Mode [Private Virtual Network]
  macaddress=52:54:00:4a:1e:d4
  qemu-system-aarch64 -smp 4 -m 8092 -M virt -bios QEMU_EFI.fd -nographic 
       -device virtio-blk-device,drive=image 
       -drive if=none,id=image,file=xenial-server-cloudimg-arm64-uefi1.qcow2,format=qcow2 
       -device virtio-blk-device,drive=cloud 
       -drive if=none,id=cloud,file=cloud.img,format=raw 
       -device virtio-net-device,netdev=network0,mac=$macaddress 
       -netdev tap,id=network0,ifname=tap0,script=qemu-ifup.sh,downscript=no  
       -enable-kvm -cpu host
fi
