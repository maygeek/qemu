#++++++  CMD

wget -c https://mirrors.tuna.tsinghua.edu.cn/centos-altarch/7.9.2009/isos/aarch64/CentOS-7-aarch64-Minimal-2009.iso
wget http://releases.linaro.org/components/kernel/uefi-linaro/16.02/release/qemu64/QEMU_EFI.fd



#install from iso
:<<!
qemu-system-aarch64 -m 2048 -cpu cortex-a57 -smp 2 -M virt -bios QEMU_EFI.fd -nographic \
-drive if=none,file=centos7-arm64.img,format=raw,id=hd0 -device virtio-blk-device,drive=hd0 \
-drive if=none,file=CentOS-7-aarch64-Minimal-2009.iso,id=cdrom,media=cdrom \
-device virtio-scsi-device -device scsi-cd,drive=cdrom

#boot from img
qemu-system-aarch64 -m 2048 -cpu cortex-a57 -smp 2 -M virt -bios QEMU_EFI.fd -nographic \
-net nic -net tap,ifname=tap0,script=no \
-drive if=none,file=centos7-arm64.img,format=raw,id=hd0 -device virtio-blk-device,drive=hd0


#=====VIRSH
apt-get install qemu-system-x86-64 libvirt-daemon qemu qemu-kvm libvirt-clients libvirt-daemon-system libvirt-daemon-system-systemd
qemu-img create -f qcow2 centos7.img 30G 
qemu-img amend -f qcow2 -o compat=0.10 centos7.img 
virsh define centos7.xml
virsh destroy
virsh undefine
virsh start centos7
virsh edit centos7
cat centos7.xml

virsh vncdisplay centos

virsh define  centos.xml
