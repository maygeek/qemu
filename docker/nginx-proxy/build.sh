cat << EOF > df 
FROM nginx:latest
COPY nginx.conf /etc/nginx/nginx.conf
COPY server.key /etc/nginx/server.key
COPY server.crt /etc/nginx/server.crt
EOF

docker build -f df -t nginx-proxy .

docker rm -f bmc-proxy
sleep 1

docker run --name bmc-proxy -d --restart unless-stopped -p 192.168.20.254:7443:7443 -p  192.168.20.254:9443:9443  --network host nginx-proxy 

docker logs -f bmc-proxy
