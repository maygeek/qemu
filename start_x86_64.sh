#sudo brctl addbr br0
#sudo ifconfig eth0 0.0.0.0
#sudo ifconfig br0 172.30.118.146 netmask 255.255.240.0
#sudo ip addr add 172.30.118.146/20 dev br0
#sudo route add default gw 172.30.112.1
#ip route add default via 172.30.112.1
#sudo ip tuntap add tap0 mode tap
#sudo ip link set tap0 master br0
#sudo ip link set eth0 master br0
#sudo brctl addif br0 tap0
#sudo brctl addif br0 eth0

# 注意 x86_64的libc在 /lib64， 而aarch64 在/lib

#创建磁盘

cat <<COMMENT
fallocate -l 4G rootfs.ext4
mkfs.ext4 rootfs.ext4
mkdir temp_root
mount -o loop rootfs.ext4 temp_root
rsync -axHAX --exclude={/dev/*,/proc/*,/sys/*,/tmp/*,/run/*,/mnt/*,/media/*,/lost+found} / temp_root/
umount temp_root
sync
COMMENT

#qemu-img create -f qcow2 disk.img 10G

:<<COMMENT
sudo qemu-system-x86_64  -smp 4 -m 4906 -nographic \
	-net nic,macaddr="14:58:d0:48:d8:f8" -net tap,ifname=tap0 \
	-kernel out/bzImage -append " console=ttyS0 earlyprintk=serial " \
	-initrd out/initrd 
	# -serial tcp::1234,server,nowait \
	#-dtb host.dtb \
-device intel-gma500 \
-device vfio-pci,host=01:00.0 \
-device virtio-gpu-pci 
COMMENT


sudo qemu-system-x86_64  -smp 4 -m 4906 -nographic \
	-net nic,macaddr="14:58:d0:48:d8:f8" -net tap,ifname=tap0 \
	-kernel out/bzImage -append "root=/dev/sda console=ttyS0 earlyprintk=serial " \
	-hda ./rootfs.ext4 
